package m1.krysinski.weather.current_weather;

import android.support.test.espresso.Espresso;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import m1.krysinski.weather.R;
import m1.krysinski.weather.data.location.model.DeviceLocation;
import m1.krysinski.weather.data.location.source.FakeLocationSourceImpl;
import m1.krysinski.weather.data.weather.source.FakeWeatherSourceImpl;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.pressKey;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

/**
 * Created by mich on 09/04/2017.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class CurrentWeatherInstrumentedTest {

    private Matcher<View> withContentDescription(final String contentDescription){
        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("has contentDescription: "+contentDescription);
            }

            @Override
            protected boolean matchesSafely(View item) {
                if(!(item instanceof ImageView)){
                    return false;
                }
                ImageView imageView = (ImageView) item;
                return TextUtils.equals(imageView.getContentDescription(), contentDescription);
            }
        };
    }

    @Rule
    public ActivityTestRule<CurrentWeatherActivity> activityActivityTestRule
            = new ActivityTestRule<CurrentWeatherActivity>(CurrentWeatherActivity.class){
        @Override
        protected void beforeActivityLaunched() {
            super.beforeActivityLaunched();
            //this is necessary because @Before executes after
            //tested activity's onResume
            FakeLocationSourceImpl.setBehavior(FakeLocationSourceImpl.Behavior.success);
            FakeLocationSourceImpl.setLocation(new DeviceLocation(0, 0));
            FakeWeatherSourceImpl.setLocationResponse(FakeWeatherSourceImpl.createLocationResponse());
            FakeWeatherSourceImpl.setQueryResponse(FakeWeatherSourceImpl.createQueryResponse());
            FakeWeatherSourceImpl.setSuccess(true);
        }

    };

    @Test
    public void testQuerySearch(){
        onView(allOf(
                isAssignableFrom(EditText.class),
                isDescendantOfA(withId(R.id.fcw_search)))).perform(clearText());
        onView(allOf(
                isAssignableFrom(EditText.class),
                isDescendantOfA(withId(R.id.fcw_search)))
        ).perform(typeText(FakeWeatherSourceImpl.VICINITY_NAME_QUERY), pressKey(KeyEvent.KEYCODE_ENTER));
        onView(withId(R.id.fcw_query_text)).check(matches(withText(FakeWeatherSourceImpl.VICINITY_NAME_QUERY)));
        onView(allOf(isDescendantOfA(withId(R.id.fcw_query_conditions)), withContentDescription(FakeWeatherSourceImpl.DESCRIPTION_QUERY))).check(matches(isDisplayed()));
        onView(withId(R.id.fcw_query_progress)).check(matches(not(isDisplayed())));
    }

    @Test
    public void testQueryDataUnavailable(){
        FakeWeatherSourceImpl.setSuccess(false);
        onView(allOf(
                isAssignableFrom(EditText.class),
                isDescendantOfA(withId(R.id.fcw_search)))).perform(clearText());
        onView(allOf(
                isAssignableFrom(EditText.class),
                isDescendantOfA(withId(R.id.fcw_search)))
        ).perform(typeText(FakeWeatherSourceImpl.VICINITY_NAME_QUERY), pressKey(KeyEvent.KEYCODE_ENTER));
        onView(withId(R.id.fcw_query_text)).check(matches(withText(R.string.message_weather_data_unavailable)));
        onView(withId(R.id.fcw_query_conditions)).check(matches(not(isDisplayed())));
        onView(withId(R.id.fcw_query_progress)).check(matches(not(isDisplayed())));
    }

    @Test
    public void testLocation(){
        onView(withId(R.id.fcw_geo_text)).check(matches(withText(FakeWeatherSourceImpl.VICINITY_NAME_LOCATION)));
        onView(withId(R.id.fcw_geo_progress)).check(matches(not(isDisplayed())));
        onView(allOf(isDescendantOfA(withId(R.id.fcw_geo_conditions)), withContentDescription(FakeWeatherSourceImpl.DESCRIPTION_LOCATION))).check(matches(isDisplayed()));
    }

    @After
    public void unregisterIdlingResource(){
        Espresso.unregisterIdlingResources(activityActivityTestRule.getActivity().getCountingIdlingResource());
    }

    @Before
    public void registerIdlingResource(){
        Espresso.registerIdlingResources(activityActivityTestRule.getActivity().getCountingIdlingResource());
    }

}
