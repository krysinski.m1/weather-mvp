package m1.krysinski.weather;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.karumi.dexter.PermissionToken;

import m1.krysinski.weather.permissions.Permission;
import m1.krysinski.weather.permissions.PermissionActions;
import m1.krysinski.weather.permissions.PermissionCallback;
import m1.krysinski.weather.permissions.PermissionDelegate;
import m1.krysinski.weather.permissions.PermissionDelegateFactoryImpl;

/**
 * Created by mich on 01/04/2017.
 */

public abstract class BasePermissionsActivity extends AppCompatActivity implements PermissionActions{

    protected PermissionDelegate permissionDelegate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        permissionDelegate = new PermissionDelegateFactoryImpl(this).createDelegate();
    }

    @Override
    public void askForPermission(final Permission permission, final PermissionCallback callback) {
        permissionDelegate.ask(permission, callback);
    }

    public void showRationale(Permission permission, final PermissionToken permissionToken){
        new AlertDialog.Builder(this)
                .setMessage(getRationaleMessage(permission))
                .setCancelable(false)
                .setPositiveButton(getRationalePositiveButtonText(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        permissionToken.continuePermissionRequest();
                    }
                })
                .setNegativeButton(getRationaleNegativeButtonText(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        permissionToken.cancelPermissionRequest();
                    }
                }).show();
    }

    protected abstract int getRationaleMessage(Permission permission);

    protected int getRationalePositiveButtonText(){
        return R.string.rationale_positive_button_text;
    }

    protected int getRationaleNegativeButtonText(){
        return R.string.rationale_negative_button_text;
    }

}
