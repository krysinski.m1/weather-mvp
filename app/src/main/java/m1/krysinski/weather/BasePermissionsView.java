package m1.krysinski.weather;

import m1.krysinski.weather.permissions.Permission;

/**
 * Created by mich on 01/04/2017.
 */

public interface BasePermissionsView<P extends BasePresenter> extends BaseView<P> {

    void onPermissionGranted(Permission permission);

    void onPermissionDenied(Permission permission);

}
