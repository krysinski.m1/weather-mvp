package m1.krysinski.weather;

/**
 * Created by mich on 27/03/2017.
 */

public interface BasePresenter<V extends BaseView> {

    void start();

    void stop();

}
