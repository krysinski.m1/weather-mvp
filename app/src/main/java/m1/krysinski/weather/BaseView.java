package m1.krysinski.weather;

/**
 * Created by mich on 27/03/2017.
 */

public interface BaseView <P extends BasePresenter> {

    void setPresenter(P presenter);

}
