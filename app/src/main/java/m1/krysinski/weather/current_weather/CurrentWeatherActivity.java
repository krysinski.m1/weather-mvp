package m1.krysinski.weather.current_weather;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;

import m1.krysinski.weather.BasePermissionsActivity;
import m1.krysinski.weather.R;
import m1.krysinski.weather.WeatherApplication;
import m1.krysinski.weather.data.location.source.LocationSourceFactoryImpl;
import m1.krysinski.weather.permissions.Permission;
import m1.krysinski.weather.util.EspressoIdlingResource;

/**
 * Created by mich on 01/04/2017.
 */

public class CurrentWeatherActivity extends BasePermissionsActivity {

    private CurrentWeatherFragment fragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_weather);
        fragment = (CurrentWeatherFragment) getSupportFragmentManager().findFragmentById(R.id.acw_fragment);
        if(fragment == null){
            fragment = CurrentWeatherFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.acw_fragment, fragment).commit();
        }
        new CurrentWeatherPresenter(
                this,
                new LocationSourceFactoryImpl(this).createLocationSource(),
                WeatherApplication.provideWeatherSource(),
                WeatherApplication.provideQuerySource(),
                fragment
        );
    }

    @Override
    protected int getRationaleMessage(Permission permission) {
        return R.string.rationale_location;
    }

    @VisibleForTesting
    public IdlingResource getCountingIdlingResource(){
        return EspressoIdlingResource.getIdlingResource();
    }

}
