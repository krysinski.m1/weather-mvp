package m1.krysinski.weather.current_weather;

import m1.krysinski.weather.BasePresenter;
import m1.krysinski.weather.BaseView;
import m1.krysinski.weather.data.weather.model.WeatherResponse;

/**
 * Created by mich on 27/03/2017.
 */

public interface CurrentWeatherContract {

    interface Presenter extends BasePresenter<View>{

        void saveLastQuery(String query);

        void loadLocationWeather();

        void loadQueryWeather(String query);

    }

    interface View extends BaseView<Presenter>{

        void showLastQuery(String lastQuery);

        void showLocationWeatherLoading();

        void showLocationWeather(WeatherResponse data);

        void showLocationWeatherUnavailable();

        void showLocationPermissionDenied();

        void showQueryWeatherLoading();

        void showQueryWeather(WeatherResponse data);

        void showQueryWeatherUnavailable();

    }

}
