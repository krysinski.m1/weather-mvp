package m1.krysinski.weather.current_weather;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import m1.krysinski.weather.R;
import m1.krysinski.weather.data.weather.model.WeatherCondition;
import m1.krysinski.weather.data.weather.model.WeatherResponse;

/**
 * Created by mich on 01/04/2017.
 */

public class CurrentWeatherFragment extends Fragment implements CurrentWeatherContract.View{

    public static CurrentWeatherFragment newInstance(){
        return new CurrentWeatherFragment();
    }

    private Unbinder unbinder;

    private CurrentWeatherContract.Presenter presenter;

    @BindView(R.id.fcw_toolbar)
    Toolbar toolbar;

    @BindView(R.id.fcw_search)
    SearchView searchView;

    @BindView(R.id.fcw_geo_text)
    TextView geoText;

    @BindView(R.id.fcw_geo_progress)
    ProgressBar geoProgress;

    @BindView(R.id.fcw_geo_conditions)
    RecyclerView geoRecycler;

    @BindView(R.id.fcw_query_panel)
    View queryPanel;

    @BindView(R.id.fcw_query_text)
    TextView queryText;

    @BindView(R.id.fcw_query_progress)
    ProgressBar queryProgress;

    @BindView(R.id.fcw_query_conditions)
    RecyclerView queryRecycler;

    private WeatherConditionsAdapter geoRecyclerAdapter;

    private WeatherConditionsAdapter queryRecyclerAdapter;

    private AppCompatActivity activity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (AppCompatActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View res = inflater.inflate(R.layout.fragment_current_weather, container, false);
        unbinder = ButterKnife.bind(this, res);
        setUpSearchView();
        setUpRecycler(geoRecycler);
        setUpRecycler(queryRecycler);
        geoRecyclerAdapter = new WeatherConditionsAdapter();
        geoRecycler.setAdapter(geoRecyclerAdapter);
        queryRecyclerAdapter = new WeatherConditionsAdapter();
        queryRecycler.setAdapter(queryRecyclerAdapter);
        return res;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
        setUpToolbar();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        deiconifySearchView();
        presenter.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.stop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setPresenter(CurrentWeatherContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showLastQuery(String lastQuery) {
        searchView.setQuery(lastQuery, false);
        deiconifySearchView();
    }

    @Override
    public void showLocationWeatherLoading() {
        geoText.setText(R.string.message_loading);
        geoProgress.setVisibility(View.VISIBLE);
        geoRecycler.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showLocationWeather(WeatherResponse data) {
        geoText.setText(data.getLocationName());
        geoProgress.setVisibility(View.INVISIBLE);
        geoRecycler.setVisibility(View.VISIBLE);
        geoRecyclerAdapter.setData(data.getWeatherConditions());
    }

    @Override
    public void showLocationWeatherUnavailable() {
        geoText.setText(R.string.message_location_unavailable);
        hideGeoProgressAndRecycler();
    }

    @Override
    public void showLocationPermissionDenied() {
        geoText.setText(R.string.message_location_permission_denied);
        hideGeoProgressAndRecycler();
    }

    @Override
    public void showQueryWeatherLoading() {
        queryPanel.setVisibility(View.VISIBLE);
        queryText.setText(R.string.message_loading);
        queryProgress.setVisibility(View.VISIBLE);
        queryRecycler.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showQueryWeather(WeatherResponse data) {
        queryPanel.setVisibility(View.VISIBLE);
        queryText.setText(data.getLocationName());
        queryProgress.setVisibility(View.INVISIBLE);
        queryRecycler.setVisibility(View.VISIBLE);
        queryRecyclerAdapter.setData(data.getWeatherConditions());
    }

    @Override
    public void showQueryWeatherUnavailable() {
        queryPanel.setVisibility(View.VISIBLE);
        queryText.setText(R.string.message_weather_data_unavailable);
        hideQueryProgressAndRecycler();
    }

    private void deiconifySearchView(){
        searchView.setIconified(false);
        searchView.clearFocus();
    }

    private void hideGeoProgressAndRecycler(){
        geoProgress.setVisibility(View.INVISIBLE);
        geoRecycler.setVisibility(View.INVISIBLE);
    }

    private void hideQueryProgressAndRecycler(){
        queryProgress.setVisibility(View.INVISIBLE);
        queryRecycler.setVisibility(View.INVISIBLE);
    }

    private void setUpSearchView(){
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                presenter.saveLastQuery(query);
                presenter.loadQueryWeather(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    private void setUpRecycler(RecyclerView recycler){
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
    }

    private void setUpToolbar(){
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private static class WeatherConditionsAdapter extends RecyclerView.Adapter<WeatherConditionsHolder>{

        private List<WeatherCondition> data;

        @Override
        public WeatherConditionsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_weather_condition, parent, false);
            return new WeatherConditionsHolder(itemView);
        }

        @Override
        public void onBindViewHolder(WeatherConditionsHolder holder, int position) {
            WeatherCondition item = data.get(position);
            holder.imageView.setContentDescription(item.getDescription());
            Picasso.with(holder.imageView.getContext()).load(Uri.parse(item.getIcon().getUrl()))
                    .noFade().into(holder.imageView);
        }

        @Override
        public int getItemCount() {
            return data == null ? 0 : data.size();
        }

        public void setData(List<WeatherCondition> data){
            this.data = data;
            notifyDataSetChanged();
        }

    }

    static class WeatherConditionsHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.vwc_image)
        ImageView imageView;

        WeatherConditionsHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
