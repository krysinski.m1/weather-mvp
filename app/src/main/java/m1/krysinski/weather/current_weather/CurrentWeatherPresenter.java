package m1.krysinski.weather.current_weather;

import m1.krysinski.weather.data.location.model.DeviceLocation;
import m1.krysinski.weather.data.location.source.LocationSource;
import m1.krysinski.weather.data.query.source.QuerySource;
import m1.krysinski.weather.data.weather.model.WeatherResponse;
import m1.krysinski.weather.data.weather.source.WeatherSource;
import m1.krysinski.weather.permissions.Permission;
import m1.krysinski.weather.permissions.PermissionActions;
import m1.krysinski.weather.permissions.PermissionCallback;
import m1.krysinski.weather.util.EspressoIdlingResource;

/**
 * Created by mich on 01/04/2017.
 */

public class CurrentWeatherPresenter implements CurrentWeatherContract.Presenter, LocationSource.LocationCallback {

    private PermissionActions permissionActions;

    private LocationSource locationSource;

    private WeatherSource weatherSource;

    private QuerySource querySource;

    private CurrentWeatherContract.View view;

    public CurrentWeatherPresenter(PermissionActions permissionActions,
                                   LocationSource locationSource,
                                   WeatherSource weatherSource,
                                   QuerySource querySource,
                                   CurrentWeatherContract.View view){
        this.permissionActions = permissionActions;
        this.locationSource = locationSource;
        this.weatherSource = weatherSource;
        this.querySource = querySource;
        this.view = view;
        view.setPresenter(this);
    }

    @Override
    public void start() {
        locationSource.addLocationCallback(this);
        loadLocationWeather();
        EspressoIdlingResource.increment();
        querySource.loadQuery(new QuerySource.LoadQueryCallback() {
            @Override
            public void onQueryLoaded(String query) {
                EspressoIdlingResource.decrement();
                if(query != null && query.length() > 0){
                    view.showLastQuery(query);
                    loadQueryWeather(query);
                }
            }
        });
    }

    @Override
    public void stop() {
        locationSource.removeLocationCallback(this);
    }

    @Override
    public void saveLastQuery(String query) {
        querySource.saveQuery(query);
    }

    @Override
    public void loadLocationWeather() {
        view.showLocationWeatherLoading();
        EspressoIdlingResource.increment();
        permissionActions.askForPermission(Permission.FINE_LOCATION, new PermissionCallback() {
            @Override
            public void onGranted(Permission permission) {
                locationSource.getLocation();
            }

            @Override
            public void onDenied(Permission permission) {
                EspressoIdlingResource.decrement();
                view.showLocationPermissionDenied();
            }
        });
    }

    @Override
    public void loadQueryWeather(String query) {
        querySource.saveQuery(query);
        view.showQueryWeatherLoading();
        EspressoIdlingResource.increment();
        weatherSource.getWeather(query, new WeatherSource.GetWeatherCallback() {
            @Override
            public void onWeatherLoaded(WeatherResponse weatherResponse) {
                EspressoIdlingResource.decrement();
                view.showQueryWeather(weatherResponse);
            }

            @Override
            public void onNoData() {
                EspressoIdlingResource.decrement();
                view.showQueryWeatherUnavailable();
            }
        });
    }

    @Override
    public void onLocationReceived(DeviceLocation location) {
        weatherSource.getWeather(location.getLatitude(), location.getLongitude(), new WeatherSource.GetWeatherCallback() {
            @Override
            public void onWeatherLoaded(WeatherResponse weatherResponse) {
                EspressoIdlingResource.decrement();
                view.showLocationWeather(weatherResponse);
            }

            @Override
            public void onNoData() {
                EspressoIdlingResource.decrement();
                view.showLocationWeatherUnavailable();
            }
        });
    }

    @Override
    public void onLocationUnavailable() {
        EspressoIdlingResource.decrement();
        view.showLocationWeatherUnavailable();
    }

    @Override
    public void onAwaitingConnection() {
        view.showLocationWeatherUnavailable();
    }
}
