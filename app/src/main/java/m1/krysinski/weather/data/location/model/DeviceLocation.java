package m1.krysinski.weather.data.location.model;

/**
 * Created by mich on 26/03/2017.
 */

public class DeviceLocation {

    private final double latitude;

    private final double longitude;

    public DeviceLocation(double latitude, double longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude(){
        return latitude;
    }

    public double getLongitude(){
        return longitude;
    }

}
