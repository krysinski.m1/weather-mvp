package m1.krysinski.weather.data.location.source;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by mich on 09/04/2017.
 */

public abstract class AbstractLocationSourceFactory implements LocationSourceFactory {

    protected AppCompatActivity activity;

    public AbstractLocationSourceFactory(AppCompatActivity activity){
        this.activity = activity;
    }

}
