package m1.krysinski.weather.data.location.source;

import java.util.HashSet;
import java.util.Set;

import m1.krysinski.weather.data.location.model.DeviceLocation;

/**
 * Created by mich on 26/03/2017.
 */

public abstract class BaseLocationSource implements LocationSource{

    protected Set<LocationCallback> callbacks;

    protected BaseLocationSource(){
        callbacks = new HashSet<>();
    }

    @Override
    public final void addLocationCallback(LocationCallback callback) {
        callbacks.add(callback);
    }

    @Override
    public final void removeLocationCallback(LocationCallback callback) {
        callbacks.remove(callback);
    }

    protected void notifyLocationReceived(DeviceLocation location) {
        for(LocationCallback c : callbacks){
            c.onLocationReceived(location);
        }
    }

    protected void notifyLocationUnavailable() {
        for(LocationCallback c : callbacks){
            c.onLocationUnavailable();
        }
    }

    protected void notifyAwaitingConnection() {
        for(LocationCallback c : callbacks){
            c.onAwaitingConnection();
        }
    }

}
