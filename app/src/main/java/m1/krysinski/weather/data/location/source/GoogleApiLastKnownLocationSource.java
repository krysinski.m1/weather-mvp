package m1.krysinski.weather.data.location.source;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import m1.krysinski.weather.data.location.model.DeviceLocation;

/**
 * Created by mich on 26/03/2017.
 */

public class GoogleApiLastKnownLocationSource extends BaseLocationSource implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private GoogleApiClient googleApiClient;

    private boolean shouldRequestLocationAfterConnected = false;

    public GoogleApiLastKnownLocationSource(AppCompatActivity activity){
        googleApiClient = new GoogleApiClient.Builder(activity)
                .enableAutoManage(activity, this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void getLocation() {
        if(googleApiClient.isConnected()){
            getLocationAndNotify();
        }else{
            shouldRequestLocationAfterConnected = true;
            notifyAwaitingConnection();
        }
    }

    @SuppressWarnings({"MissingPermission"})
    private void getLocationAndNotify(){
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if(location != null){
            notifyLocationReceived(new DeviceLocation(location.getLatitude(), location.getLongitude()));
        }else{
            notifyLocationUnavailable();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if(shouldRequestLocationAfterConnected){
            shouldRequestLocationAfterConnected = false;
            getLocationAndNotify();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        //no - op
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        notifyLocationUnavailable();
    }
}
