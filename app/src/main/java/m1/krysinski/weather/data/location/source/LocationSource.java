package m1.krysinski.weather.data.location.source;

import m1.krysinski.weather.data.location.model.DeviceLocation;

/**
 * Created by mich on 26/03/2017.
 */

public interface LocationSource {

    void getLocation();

    void addLocationCallback(LocationCallback callback);

    void removeLocationCallback(LocationCallback callback);

    interface LocationCallback{

        void onLocationReceived(DeviceLocation location);

        void onLocationUnavailable();

        void onAwaitingConnection();

    }

}
