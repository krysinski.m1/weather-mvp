package m1.krysinski.weather.data.location.source;

/**
 * Created by mich on 09/04/2017.
 */

public interface LocationSourceFactory {

    LocationSource createLocationSource();

}
