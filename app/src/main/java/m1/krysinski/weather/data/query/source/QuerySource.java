package m1.krysinski.weather.data.query.source;

/**
 * Created by mich on 02/04/2017.
 */

public interface QuerySource {

    void saveQuery(String query);

    void loadQuery(LoadQueryCallback callback);

    interface LoadQueryCallback{

        void onQueryLoaded(String query);

    }

}
