package m1.krysinski.weather.data.query.source;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by mich on 02/04/2017.
 */

public class SharedPreferencesQuerySource implements QuerySource {

    private static final String KEY_QUERY = "shared.preferences.query.source.query.key";

    private SharedPreferences preferences;

    private Handler handler;

    private ExecutorService executor;

    public SharedPreferencesQuerySource(Context context){
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        handler = new Handler(Looper.getMainLooper());
        executor = Executors.newSingleThreadExecutor();
    }

    @Override
    public void saveQuery(String query) {
        preferences.edit().putString(KEY_QUERY, query).apply();
    }

    @Override
    public void loadQuery(final LoadQueryCallback callback) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                postResult(callback, preferences.getString(KEY_QUERY, null));
            }
        });
    }

    private void postResult(final LoadQueryCallback callback, final String query){
        handler.post(new Runnable() {
            @Override
            public void run() {
                callback.onQueryLoaded(query);
            }
        });
    }

}
