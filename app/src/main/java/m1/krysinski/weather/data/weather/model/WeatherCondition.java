package m1.krysinski.weather.data.weather.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mich on 25/03/2017.
 */

public class WeatherCondition {

    @Expose
    private long id;

    @Expose
    @SerializedName("main")
    private String name;

    @Expose
    private String description;

    @Expose
    private WeatherIcon icon;

    @SuppressWarnings("unused")
    public WeatherCondition(){}

    public WeatherCondition(long id, String name, String description, WeatherIcon icon){
        this.id = id;
        this.name = name;
        this.description = description;
        this.icon = icon;
    }

    public long getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public String getDescription(){
        return description;
    }

    public WeatherIcon getIcon(){
        return icon;
    }

}
