package m1.krysinski.weather.data.weather.model;

/**
 * Created by mich on 02/04/2017.
 */

public class WeatherIcon {

    private String id;

    private String url;

    @SuppressWarnings("unused")
    public WeatherIcon(){}

    public WeatherIcon(String id, String url){
        this.id = id;
        this.url = url;
    }

    public String getId(){
        return id;
    }

    public String getUrl(){
        return url;
    }

}
