package m1.krysinski.weather.data.weather.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mich on 25/03/2017.
 */

public class WeatherResponse {

    @Expose
    @SerializedName("name")
    private String locationName;

    @Expose
    @SerializedName("weather")
    private List<WeatherCondition> weatherConditions;

    @SuppressWarnings("unused")
    public WeatherResponse(){}

    public WeatherResponse(String locationName, List<WeatherCondition> weatherConditions){
        this.locationName = locationName;
        this.weatherConditions = weatherConditions;
    }

    public String getLocationName(){
        return locationName;
    }

    public List<WeatherCondition> getWeatherConditions(){
        return weatherConditions;
    }

}
