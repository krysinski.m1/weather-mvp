package m1.krysinski.weather.data.weather.service;

import m1.krysinski.weather.data.weather.model.WeatherResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by mich on 25/03/2017.
 */

public interface WeatherService {

    @GET("weather")
    Call<WeatherResponse> getWeather(@Query("lat") double latitude, @Query("lon")double longitude);

    @GET("weather")
    Call<WeatherResponse> getWeather(@Query("q") String query);

}
