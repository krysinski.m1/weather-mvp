package m1.krysinski.weather.data.weather.source;

import m1.krysinski.weather.data.weather.model.WeatherResponse;

/**
 * Created by mich on 25/03/2017.
 */

public interface WeatherSource {

    interface GetWeatherCallback{

        void onWeatherLoaded(WeatherResponse weatherResponse);

        void onNoData();

    }

    void getWeather(String query, GetWeatherCallback callback);

    void getWeather(double latitude, double longitude, GetWeatherCallback callback);

}
