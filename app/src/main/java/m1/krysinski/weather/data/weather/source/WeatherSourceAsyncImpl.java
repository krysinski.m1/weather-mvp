package m1.krysinski.weather.data.weather.source;

import m1.krysinski.weather.data.weather.model.WeatherResponse;
import m1.krysinski.weather.data.weather.service.WeatherService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mich on 26/03/2017.
 */

public class WeatherSourceAsyncImpl implements WeatherSource {

    private WeatherService weatherService;

    private Call<WeatherResponse> queryCall;

    private Call<WeatherResponse> locationCall;

    public WeatherSourceAsyncImpl(WeatherService weatherService){
        this.weatherService = weatherService;
    }

    @Override
    public void getWeather(String query, GetWeatherCallback callback) {
        if(queryCall != null){
            queryCall.cancel();
        }
        queryCall = weatherService.getWeather(query);
        queryCall.enqueue(new RetrofitCallback(callback, new AfterCall() {
            @Override
            void doAfterCall() {
                queryCall = null;
            }
        }));
    }

    @Override
    public void getWeather(double latitude, double longitude, GetWeatherCallback callback) {
        if(locationCall != null){
            locationCall.cancel();
        }
        locationCall = weatherService.getWeather(latitude, longitude);
        locationCall.enqueue(new RetrofitCallback(callback, new AfterCall() {
            @Override
            void doAfterCall() {
                locationCall = null;
            }
        }));
    }

    private class RetrofitCallback implements Callback<WeatherResponse>{

        private GetWeatherCallback getWeatherCallback;

        private AfterCall afterCall;

        private RetrofitCallback(GetWeatherCallback getWeatherCallback, AfterCall afterCall){
            this.getWeatherCallback = getWeatherCallback;
            this.afterCall = afterCall;
        }

        @Override
        public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
            if(response.isSuccessful()){
                getWeatherCallback.onWeatherLoaded(response.body());
            }else{
                getWeatherCallback.onNoData();
            }
            afterCall.doAfterCall();
        }

        @Override
        public void onFailure(Call<WeatherResponse> call, Throwable t) {
            if(!call.isCanceled()){
                getWeatherCallback.onNoData();
            }
            afterCall.doAfterCall();
        }

    }

    private abstract class AfterCall{

        abstract void doAfterCall();

    }

}
