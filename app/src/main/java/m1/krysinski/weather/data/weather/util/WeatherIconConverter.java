package m1.krysinski.weather.data.weather.util;

/**
 * Created by mich on 02/04/2017.
 */

public interface WeatherIconConverter {

    String getIconUrl(String iconId);

}
