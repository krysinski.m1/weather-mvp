package m1.krysinski.weather.data.weather.util;

import java.util.Locale;

/**
 * Created by mich on 02/04/2017.
 */

public class WeatherIconConverterImpl implements WeatherIconConverter {

    private String template;

    public WeatherIconConverterImpl(String template){
        this.template = template;
    }

    @Override
    public String getIconUrl(String iconId) {
        return String.format(Locale.ENGLISH, template, iconId);
    }

}
