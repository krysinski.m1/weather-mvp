package m1.krysinski.weather.data.weather.util;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import m1.krysinski.weather.data.weather.model.WeatherIcon;

/**
 * Created by mich on 02/04/2017.
 */

public class WeatherIconDeserializer implements JsonDeserializer<WeatherIcon> {

    private WeatherIconConverter converter;

    public WeatherIconDeserializer(WeatherIconConverter converter){
        this.converter = converter;
    }

    @Override
    public WeatherIcon deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String iconId = json.getAsString();
        return new WeatherIcon(iconId, converter.getIconUrl(iconId));
    }

}
