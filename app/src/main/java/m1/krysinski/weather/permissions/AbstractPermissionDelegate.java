package m1.krysinski.weather.permissions;

import m1.krysinski.weather.BasePermissionsActivity;

/**
 * Created by mich on 09/04/2017.
 */

public abstract class AbstractPermissionDelegate implements PermissionDelegate {

    protected BasePermissionsActivity activity;

    public AbstractPermissionDelegate(BasePermissionsActivity activity){
        this.activity = activity;
    }

}
