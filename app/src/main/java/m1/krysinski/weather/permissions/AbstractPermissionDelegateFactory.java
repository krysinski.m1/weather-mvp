package m1.krysinski.weather.permissions;

import m1.krysinski.weather.BasePermissionsActivity;

/**
 * Created by mich on 09/04/2017.
 */

public abstract class AbstractPermissionDelegateFactory implements PermissionDelegateFactory{

    protected BasePermissionsActivity activity;

    public AbstractPermissionDelegateFactory(BasePermissionsActivity activity){
        this.activity = activity;
    }

}
