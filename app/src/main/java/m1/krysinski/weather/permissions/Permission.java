package m1.krysinski.weather.permissions;

import android.Manifest;

/**
 * Created by mich on 27/03/2017.
 */

public enum Permission {

    FINE_LOCATION(Manifest.permission.ACCESS_FINE_LOCATION);

    private String name;

    Permission(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

}
