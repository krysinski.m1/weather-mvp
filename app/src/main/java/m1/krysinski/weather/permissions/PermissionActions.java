package m1.krysinski.weather.permissions;

/**
 * Created by mich on 01/04/2017.
 */

public interface PermissionActions {

    void askForPermission(Permission permission, PermissionCallback callback);

}
