package m1.krysinski.weather.permissions;

/**
 * Created by mich on 01/04/2017.
 */

public interface PermissionCallback {

    void onGranted(Permission permission);

    void onDenied(Permission permission);

}
