package m1.krysinski.weather.permissions;

/**
 * Created by mich on 09/04/2017.
 */

public interface PermissionDelegate {

    void ask(Permission permission, PermissionCallback callback);

}
