package m1.krysinski.weather.permissions;

/**
 * Created by mich on 09/04/2017.
 */

public interface PermissionDelegateFactory {

    PermissionDelegate createDelegate();

}
