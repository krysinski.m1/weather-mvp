package m1.krysinski.weather.util;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by mich on 25/03/2017.
 */

public class WeatherApiInterceptor implements Interceptor {

    static final String Q_APP_ID = "APPID";

    private final String appId;

    public WeatherApiInterceptor(String appId){
        this.appId = appId;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        HttpUrl url = request.url();
        url = url.newBuilder()
                .addQueryParameter(Q_APP_ID, appId)
                .build();
        request = request.newBuilder().url(url).build();
        return chain.proceed(request);
    }

}
