package m1.krysinski.weather;

import android.app.Application;

import m1.krysinski.weather.data.query.source.QuerySource;
import m1.krysinski.weather.data.query.source.SharedPreferencesQuerySource;
import m1.krysinski.weather.data.weather.source.FakeWeatherSourceImpl;
import m1.krysinski.weather.data.weather.source.WeatherSource;

/**
 * Created by mich on 09/04/2017.
 */

public class WeatherApplication extends Application {

    private static QuerySource querySource;

    @Override
    public void onCreate() {
        super.onCreate();
        querySource = new SharedPreferencesQuerySource(this);
    }

    public static WeatherSource provideWeatherSource(){
        return new FakeWeatherSourceImpl();
    }

    public static QuerySource provideQuerySource(){
        return querySource;
    }

}
