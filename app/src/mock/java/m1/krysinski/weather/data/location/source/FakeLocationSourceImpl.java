package m1.krysinski.weather.data.location.source;

import android.support.annotation.VisibleForTesting;

import m1.krysinski.weather.data.location.model.DeviceLocation;

/**
 * Created by mich on 09/04/2017.
 */

public class FakeLocationSourceImpl implements LocationSource {

    private static LocationCallback callback;

    private static Behavior behavior = Behavior.success;

    private static DeviceLocation location = new DeviceLocation(0, 0);

    @Override
    public void getLocation() {
        switch(behavior){
            case success:
                callback.onLocationReceived(location);
                break;
            case fail:
                callback.onLocationUnavailable();
                break;
            case wait:
                callback.onAwaitingConnection();
                break;
        }
    }

    @Override
    public void addLocationCallback(LocationCallback callback) {
        FakeLocationSourceImpl.callback = callback;
    }

    @Override
    public void removeLocationCallback(LocationCallback callback) {
        FakeLocationSourceImpl.callback = null;
    }

    @VisibleForTesting
    public static void setBehavior(Behavior behavior){
        FakeLocationSourceImpl.behavior = behavior;
    }

    @VisibleForTesting
    public static void setLocation(DeviceLocation location){
        FakeLocationSourceImpl.location = location;
    }

    public enum Behavior{
        success, fail, wait
    }

}
