package m1.krysinski.weather.data.location.source;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by mich on 09/04/2017.
 */

public class LocationSourceFactoryImpl extends AbstractLocationSourceFactory {

    private AppCompatActivity activity;

    public LocationSourceFactoryImpl(AppCompatActivity activity){
        super(activity);
    }

    public LocationSource createLocationSource(){
        return new FakeLocationSourceImpl();
    }

}
