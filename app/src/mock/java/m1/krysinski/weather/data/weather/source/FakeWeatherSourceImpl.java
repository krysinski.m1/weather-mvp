package m1.krysinski.weather.data.weather.source;

import android.support.annotation.VisibleForTesting;

import java.util.ArrayList;
import java.util.List;

import m1.krysinski.weather.data.weather.model.WeatherCondition;
import m1.krysinski.weather.data.weather.model.WeatherIcon;
import m1.krysinski.weather.data.weather.model.WeatherResponse;

/**
 * Created by mich on 09/04/2017.
 */

public class FakeWeatherSourceImpl implements WeatherSource {

    public static final String VICINITY_NAME_LOCATION = "Amsterdam";

    public static final String VICINITY_NAME_QUERY = "Heesch";

    public static final String DESCRIPTION_LOCATION = "clear sky";

    public static final String DESCRIPTION_QUERY = "few clouds";

    private static WeatherResponse locationResponse = createLocationResponse();

    private static WeatherResponse queryResponse = createQueryResponse();

    private static boolean success = true;

    @Override
    public void getWeather(String query, GetWeatherCallback callback) {
        if(success){
            callback.onWeatherLoaded(queryResponse);
        }else{
            callback.onNoData();
        }
    }

    @Override
    public void getWeather(double latitude, double longitude, GetWeatherCallback callback) {
        if(success){
            callback.onWeatherLoaded(locationResponse);
        }else{
            callback.onNoData();
        }
    }

    @VisibleForTesting
    public static void setLocationResponse(WeatherResponse weatherResponse) {
        FakeWeatherSourceImpl.locationResponse = weatherResponse;
    }

    @VisibleForTesting
    public static void setQueryResponse(WeatherResponse weatherResponse) {
        FakeWeatherSourceImpl.queryResponse = weatherResponse;
    }

    @VisibleForTesting
    public static void setSuccess(boolean success){
        FakeWeatherSourceImpl.success = success;
    }

    public static WeatherResponse createLocationResponse() {
        WeatherIcon icon = new WeatherIcon("01d.png", "file:///android_asset/01d.png");
        WeatherCondition condition = new WeatherCondition(1L, DESCRIPTION_LOCATION, DESCRIPTION_LOCATION, icon);
        List<WeatherCondition> conditions = new ArrayList<>();
        conditions.add(condition);
        return new WeatherResponse(VICINITY_NAME_LOCATION, conditions);
    }

    public static WeatherResponse createQueryResponse(){
        WeatherIcon icon = new WeatherIcon("02d.png", "file:///android_asset/02d.png");
        WeatherCondition condition = new WeatherCondition(1L, DESCRIPTION_QUERY, DESCRIPTION_QUERY, icon);
        List<WeatherCondition> conditions = new ArrayList<>();
        conditions.add(condition);
        return new WeatherResponse(VICINITY_NAME_QUERY, conditions);
    }

}
