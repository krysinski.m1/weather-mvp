package m1.krysinski.weather.permissions;

/**
 * Created by mich on 09/04/2017.
 */

public class FakePermissionDelegate implements PermissionDelegate {

    @Override
    public void ask(Permission permission, PermissionCallback callback) {
        callback.onGranted(permission);
    }

}
