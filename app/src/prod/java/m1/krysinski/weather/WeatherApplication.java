package m1.krysinski.weather;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.picasso.Picasso;

import m1.krysinski.weather.data.query.source.QuerySource;
import m1.krysinski.weather.data.query.source.SharedPreferencesQuerySource;
import m1.krysinski.weather.data.weather.model.WeatherIcon;
import m1.krysinski.weather.data.weather.service.WeatherService;
import m1.krysinski.weather.data.weather.source.WeatherSource;
import m1.krysinski.weather.data.weather.source.WeatherSourceAsyncImpl;
import m1.krysinski.weather.data.weather.util.WeatherIconConverterImpl;
import m1.krysinski.weather.data.weather.util.WeatherIconDeserializer;
import m1.krysinski.weather.util.WeatherApiInterceptor;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mich on 25/03/2017.
 */

public class WeatherApplication extends Application {

    private static WeatherSource weatherSource;

    private static QuerySource querySource;

    @Override
    public void onCreate() {
        super.onCreate();
        if(LeakCanary.isInAnalyzerProcess(this)){
            return;
        }
        LeakCanary.install(this);
        querySource = new SharedPreferencesQuerySource(this);
        Cache cache = new Cache(getCacheDir(), getResources().getInteger(R.integer.cache_size_megabytes) * 1024 * 1024);
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(new WeatherApiInterceptor(getString(R.string.weather_api_key)))
                .build();
        Gson gson = new GsonBuilder().registerTypeAdapter(WeatherIcon.class,
                new WeatherIconDeserializer(new WeatherIconConverterImpl(getString(R.string.weather_icon_template))))
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(getString(R.string.weather_base_url))
                .build();
        Picasso picasso = new Picasso.Builder(this).downloader(new OkHttp3Downloader(httpClient)).build();
        Picasso.setSingletonInstance(picasso);
        WeatherService weatherService = retrofit.create(WeatherService.class);
        weatherSource = new WeatherSourceAsyncImpl(weatherService);
    }

    public static WeatherSource provideWeatherSource(){
        return weatherSource;
    }

    public static QuerySource provideQuerySource() {
        return querySource;
    }

}
