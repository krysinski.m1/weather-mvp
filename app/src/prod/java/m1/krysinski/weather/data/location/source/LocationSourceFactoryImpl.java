package m1.krysinski.weather.data.location.source;

import android.support.v7.app.AppCompatActivity;

import m1.krysinski.weather.data.location.source.AbstractLocationSourceFactory;
import m1.krysinski.weather.data.location.source.GoogleApiLastKnownLocationSource;
import m1.krysinski.weather.data.location.source.LocationSource;

/**
 * Created by mich on 09/04/2017.
 */

public class LocationSourceFactoryImpl extends AbstractLocationSourceFactory{

    public LocationSourceFactoryImpl(AppCompatActivity activity) {
        super(activity);
    }

    @Override
    public LocationSource createLocationSource() {
        return new GoogleApiLastKnownLocationSource(activity);
    }

}
