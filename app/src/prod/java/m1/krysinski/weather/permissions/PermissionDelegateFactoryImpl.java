package m1.krysinski.weather.permissions;

import m1.krysinski.weather.BasePermissionsActivity;

/**
 * Created by mich on 09/04/2017.
 */

public class PermissionDelegateFactoryImpl extends AbstractPermissionDelegateFactory{

    public PermissionDelegateFactoryImpl(BasePermissionsActivity activity) {
        super(activity);
    }

    @Override
    public PermissionDelegate createDelegate() {
        return new PermissionDelegateImpl(activity);
    }
}
