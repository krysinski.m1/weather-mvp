package m1.krysinski.weather.permissions;


import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import m1.krysinski.weather.BasePermissionsActivity;

/**
 * Created by mich on 09/04/2017.
 */

public class PermissionDelegateImpl extends AbstractPermissionDelegate {

    public PermissionDelegateImpl(BasePermissionsActivity activity){
        super(activity);
    }

    @Override
    public void ask(final Permission permission, final PermissionCallback callback) {
        Dexter.withActivity(activity).withPermission(permission.getName()).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                callback.onGranted(permission);
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                callback.onDenied(permission);
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken token) {
                activity.showRationale(permission, token);
            }
        }).check();
    }

}
