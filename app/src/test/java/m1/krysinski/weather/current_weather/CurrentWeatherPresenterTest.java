package m1.krysinski.weather.current_weather;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import m1.krysinski.weather.data.location.source.LocationSource;
import m1.krysinski.weather.data.query.source.QuerySource;
import m1.krysinski.weather.data.weather.model.WeatherResponse;
import m1.krysinski.weather.data.weather.source.WeatherSource;
import m1.krysinski.weather.permissions.Permission;
import m1.krysinski.weather.permissions.PermissionActions;
import m1.krysinski.weather.permissions.PermissionCallback;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * Created by mich on 08/04/2017.
 */

public class CurrentWeatherPresenterTest {

    @Mock
    private PermissionActions permissionActions;

    @Mock
    private WeatherSource weatherSource;

    @Mock
    private QuerySource querySource;

    @Mock
    private LocationSource fakeLocationSource;

    @Mock
    private CurrentWeatherContract.View view;

    @Captor
    private ArgumentCaptor<PermissionCallback> permissionCallbackArgumentCaptor;

    @Captor
    private ArgumentCaptor<LocationSource.LocationCallback> locationCallbackArgumentCaptor;

    @Captor
    private ArgumentCaptor<WeatherSource.GetWeatherCallback> weatherCallbackArgumentCaptor;

    @Captor
    private ArgumentCaptor<QuerySource.LoadQueryCallback> queryCallbackArgumentCaptor;



    private CurrentWeatherPresenter presenter;

    @Before
    public void setUpPresenter(){
        MockitoAnnotations.initMocks(this);
        presenter = Mockito.spy(new CurrentWeatherPresenter(permissionActions, fakeLocationSource, weatherSource, querySource, view));
    }

    @Test
    public void testPermissionDenied(){
        presenter.loadLocationWeather();
        verify(view).showLocationWeatherLoading();
        verify(permissionActions).askForPermission(eq(Permission.FINE_LOCATION), permissionCallbackArgumentCaptor.capture());
        permissionCallbackArgumentCaptor.getValue().onDenied(Permission.FINE_LOCATION);
        verify(view).showLocationPermissionDenied();
    }

    @Test
    public void testQueryWeatherDataNotAvailable(){
        presenter.loadQueryWeather("Heesch");
        verify(querySource).saveQuery(eq("Heesch"));
        verify(view).showQueryWeatherLoading();
        verify(weatherSource).getWeather(eq("Heesch"), weatherCallbackArgumentCaptor.capture());
        weatherCallbackArgumentCaptor.getValue().onNoData();
        verify(view).showQueryWeatherUnavailable();
    }

    @Test
    public void testQueryWeatherDataReceived(){
        presenter.loadQueryWeather("Heesch");
        verify(querySource).saveQuery(eq("Heesch"));
        verify(view).showQueryWeatherLoading();
        verify(weatherSource).getWeather(eq("Heesch"), weatherCallbackArgumentCaptor.capture());
        WeatherResponse weatherResponse = Mockito.mock(WeatherResponse.class);
        weatherCallbackArgumentCaptor.getValue().onWeatherLoaded(weatherResponse);
        verify(view).showQueryWeather(eq(weatherResponse));
    }

}
