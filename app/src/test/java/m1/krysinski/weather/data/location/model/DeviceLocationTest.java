package m1.krysinski.weather.data.location.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by mich on 03.04.17.
 */

public class DeviceLocationTest {

    @Test
    public void deviceLocation_valuesAreCorrect() throws Exception {
        DeviceLocation deviceLocation = new DeviceLocation(40, 50);
        assertEquals(40, deviceLocation.getLatitude(), 0.001);
        assertEquals(50, deviceLocation.getLongitude(), 0.001);
    }

}
