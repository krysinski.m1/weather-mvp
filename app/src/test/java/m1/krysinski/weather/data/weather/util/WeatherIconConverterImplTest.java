package m1.krysinski.weather.data.weather.util;

import junit.framework.Assert;

import org.junit.Test;

/**
 * Created by mich on 03.04.17.
 */

public class WeatherIconConverterImplTest {

    @Test
    public void verify() throws Exception {
        final String iconId = "iconId";
        final String template = "test %s test";
        final String expected = "test iconId test";
        WeatherIconConverter converter = new WeatherIconConverterImpl(template);
        String iconUrl = converter.getIconUrl(iconId);
        Assert.assertEquals(expected, iconUrl);
    }

}
