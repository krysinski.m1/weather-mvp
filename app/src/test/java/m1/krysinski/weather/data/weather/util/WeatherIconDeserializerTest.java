package m1.krysinski.weather.data.weather.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import junit.framework.Assert;

import org.junit.Test;

import m1.krysinski.weather.data.weather.model.WeatherIcon;

/**
 * Created by mich on 03.04.17.
 */

public class WeatherIconDeserializerTest {

    @Test
    public void testDeserializer() throws Exception {
        final String json = "expected";
        final String expetedId = "expected";
        final String expectedUrl = "test expected";
        final String template = "test %s";
        Gson gson = new GsonBuilder().registerTypeAdapter(WeatherIcon.class,
                new WeatherIconDeserializer(new WeatherIconConverterImpl(template))).create();
        WeatherIcon icon = gson.fromJson(json, WeatherIcon.class);
        Assert.assertEquals(icon.getId(), expetedId);
        Assert.assertEquals(icon.getUrl(), expectedUrl);
    }

}
