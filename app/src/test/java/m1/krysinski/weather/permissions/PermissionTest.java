package m1.krysinski.weather.permissions;

import android.Manifest;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by mich on 03.04.17.
 */

public class PermissionTest {

    @Test
    public void testPermission() throws Exception {
        Assert.assertThat(Permission.FINE_LOCATION.getName(), CoreMatchers.is(Manifest.permission.ACCESS_FINE_LOCATION));
        Assert.assertThat(Permission.values(), CoreMatchers.is(new Permission[]{Permission.FINE_LOCATION}));
        Assert.assertThat(Permission.valueOf("FINE_LOCATION"), CoreMatchers.is(Permission.FINE_LOCATION));
    }

}
