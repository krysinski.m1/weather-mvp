package m1.krysinski.weather.util;


import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

/**
 * Created by mich on 03.04.17.
 */

public class WeatherApiInterceptorTest {

    @Test
    public void testApiInterceptro() throws Exception {
        final String apiKey = "expect_this";
        final String expected = WeatherApiInterceptor.Q_APP_ID+"="+apiKey;
        MockWebServer mockWebServer = new MockWebServer();
        mockWebServer.enqueue(new MockResponse());
        mockWebServer.start();

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .addInterceptor(new WeatherApiInterceptor(apiKey)).build();
        okHttpClient.newCall(new Request.Builder().url(mockWebServer.url("/")).build()).execute();

        RecordedRequest request = mockWebServer.takeRequest();
        Assert.assertThat(request.getRequestLine(), CoreMatchers.containsString(expected));

        mockWebServer.shutdown();
    }

}
