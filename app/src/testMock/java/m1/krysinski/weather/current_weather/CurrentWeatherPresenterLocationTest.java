package m1.krysinski.weather.current_weather;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import m1.krysinski.weather.data.location.model.DeviceLocation;
import m1.krysinski.weather.data.location.source.FakeLocationSourceImpl;
import m1.krysinski.weather.data.location.source.LocationSource;
import m1.krysinski.weather.data.query.source.QuerySource;
import m1.krysinski.weather.data.weather.model.WeatherResponse;
import m1.krysinski.weather.data.weather.source.WeatherSource;
import m1.krysinski.weather.permissions.Permission;
import m1.krysinski.weather.permissions.PermissionActions;
import m1.krysinski.weather.permissions.PermissionCallback;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * Created by mich on 09/04/2017.
 */

public class CurrentWeatherPresenterLocationTest {

    @Mock
    private PermissionActions permissionActions;

    @Mock
    private WeatherSource weatherSource;

    @Mock
    private QuerySource querySource;

    @Mock
    private CurrentWeatherContract.View view;

    @Captor
    private ArgumentCaptor<PermissionCallback> permissionCallbackArgumentCaptor;

    @Captor
    private ArgumentCaptor<LocationSource.LocationCallback> locationCallbackArgumentCaptor;

    @Captor
    private ArgumentCaptor<WeatherSource.GetWeatherCallback> weatherCallbackArgumentCaptor;

    @Captor
    private ArgumentCaptor<QuerySource.LoadQueryCallback> queryCallbackArgumentCaptor;

    private CurrentWeatherPresenter presenter;

    @Spy
    private FakeLocationSourceImpl fakeLocationSource = new FakeLocationSourceImpl();

    @Before
    public void setUpPresenter(){
        MockitoAnnotations.initMocks(this);
        presenter = Mockito.spy(new CurrentWeatherPresenter(permissionActions, fakeLocationSource, weatherSource, querySource, view));
        FakeLocationSourceImpl.setBehavior(FakeLocationSourceImpl.Behavior.success);
        FakeLocationSourceImpl.setLocation(new DeviceLocation(0, 0));
    }

    @Test
    public void testLocationAwait(){
        FakeLocationSourceImpl.setBehavior(FakeLocationSourceImpl.Behavior.wait);
        fakeLocationSource.addLocationCallback(presenter);
        presenter.loadLocationWeather();
        verify(view).showLocationWeatherLoading();
        verify(permissionActions).askForPermission(eq(Permission.FINE_LOCATION), permissionCallbackArgumentCaptor.capture());
        permissionCallbackArgumentCaptor.getValue().onGranted(Permission.FINE_LOCATION);
        verify(fakeLocationSource).getLocation();
        verify(presenter).onAwaitingConnection();
        verify(view).showLocationWeatherUnavailable();
    }

    @Test
    public void testLocationUnavailable(){
        FakeLocationSourceImpl.setBehavior(FakeLocationSourceImpl.Behavior.fail);
        fakeLocationSource.addLocationCallback(presenter);
        presenter.loadLocationWeather();
        verify(view).showLocationWeatherLoading();
        verify(permissionActions).askForPermission(eq(Permission.FINE_LOCATION), permissionCallbackArgumentCaptor.capture());
        permissionCallbackArgumentCaptor.getValue().onGranted(Permission.FINE_LOCATION);
        verify(fakeLocationSource).getLocation();
        verify(presenter).onLocationUnavailable();
        verify(view).showLocationWeatherUnavailable();
    }

    @Test
    public void testLocationReceivedDataUnavailable(){
        fakeLocationSource.addLocationCallback(presenter);
        presenter.loadLocationWeather();
        verify(view).showLocationWeatherLoading();
        verify(permissionActions).askForPermission(eq(Permission.FINE_LOCATION), permissionCallbackArgumentCaptor.capture());
        permissionCallbackArgumentCaptor.getValue().onGranted(Permission.FINE_LOCATION);
        verify(fakeLocationSource).getLocation();
        verify(presenter).onLocationReceived(any(DeviceLocation.class));
        verify(weatherSource).getWeather(anyDouble(), anyDouble(), weatherCallbackArgumentCaptor.capture());
        weatherCallbackArgumentCaptor.getValue().onNoData();
        verify(view).showLocationWeatherUnavailable();
    }

    @Test
    public void testLocationReceivedDataReceived(){
        fakeLocationSource.addLocationCallback(presenter);
        presenter.loadLocationWeather();
        verify(view).showLocationWeatherLoading();
        verify(permissionActions).askForPermission(eq(Permission.FINE_LOCATION), permissionCallbackArgumentCaptor.capture());
        permissionCallbackArgumentCaptor.getValue().onGranted(Permission.FINE_LOCATION);
        verify(fakeLocationSource).getLocation();
        verify(presenter).onLocationReceived(any(DeviceLocation.class));
        verify(weatherSource).getWeather(anyDouble(), anyDouble(), weatherCallbackArgumentCaptor.capture());
        WeatherResponse weatherResponse = Mockito.mock(WeatherResponse.class);
        weatherCallbackArgumentCaptor.getValue().onWeatherLoaded(weatherResponse);
        verify(view).showLocationWeather(eq(weatherResponse));
    }

}
